﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpriteMap
{
    public class HeightMapGenerator
    {

        Bitmap currentBitmap;
        Color bgColour;

        private int getDistFromBG(int x, int y)
        {
            int d = 0;
            double step = 10;
            while (true)
            {
                bool checkedOne = false;
                for (double a = 0; a <= 360.0; a += step)
                {
                    int xx = (int)(Math.Sin(a) * d) + x;
                    int yy = (int)(Math.Cos(a) * d) + y;

                    if (xx >= 0 && xx < currentBitmap.Width && yy >= 0 && yy < currentBitmap.Height)
                    {
                        checkedOne = true;
                        if (currentBitmap.GetPixel(xx, yy) == bgColour)
                            return d;
                    }
                }
                d++;
                if (!checkedOne)
                    break;
            }
            return 0;
        }

        float lerp(float t,float a,float b) 
        {
            return a + t * (b - a);
        }

        public float[,] smooth(float[,] map, float degree = 0.5f)
        {
            float[,] result = new float[map.GetLength(0), map.GetLength(1)];
            for (int x = 1; x < map.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < map.GetLength(1) - 1; y++)
                {
                    var av = (map[x - 1, y - 1] + map[x, y - 1] + map[x + 1, y - 1] + map[x - 1, y] + map[x, y] + map[x + 1, y] + map[x - 1, y + 1] + map[x, y + 1] + map[x + 1, y + 1]) / 9;
                    result[x,y] = lerp(degree, map[x, y], av); 
                }
            }
            return result;
        }

        public Image GenerateHeightMap(Image source, float blend = 0.5f)
        {
            currentBitmap = (Bitmap)source;
            bgColour = currentBitmap.GetPixel(0, 0);
            int[,] distFromBG = new int[currentBitmap.Width, currentBitmap.Height];

            for (int x = 0; x < currentBitmap.Width; x++)
            {
                for (int y = 0; y < currentBitmap.Height; y++)
                {
                    distFromBG[x,y] = getDistFromBG(x, y);
                }   
            }

            int maxValue = distFromBG.Cast<int>().ToArray().Max();

            float[,] map = new float[currentBitmap.Width, currentBitmap.Height];

            for (int x = 0; x < currentBitmap.Width; x++)
            {
                for (int y = 0; y < currentBitmap.Height; y++)
                {
                    map[x, y] = (float)distFromBG[x, y] / (float)maxValue;
                }
            }

            map = smooth(map, 1f);
            map = smooth(map, 1f);
            map = smooth(map, 1f);
            map = smooth(map, 1f);
            map = smooth(map, 1f);
            map = smooth(map, 1f);

            Bitmap heightMap = new Bitmap(source.Width, source.Height);

            for (int x = 0; x < currentBitmap.Width; x++)
            {
                for (int y = 0; y < currentBitmap.Height; y++)
                {
                    Color currentCol = currentBitmap.GetPixel(x, y);
                    if (currentCol == bgColour)
                    {
                        heightMap.SetPixel(x, y, Color.Black);
                    }
                    else
                    {
                        float h = lerp(blend, map[x,y], currentCol.GetBrightness());
                        int g = (int)(h * 255f);
                        Color col = Color.FromArgb(255, g, g, g);
                        heightMap.SetPixel(x, y, col);
                    }
                    
                }
            }

            return (Image)heightMap;

        }

    }
}
