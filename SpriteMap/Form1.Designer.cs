﻿namespace SpriteMap
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.originalPictureBox = new System.Windows.Forms.PictureBox();
            this.heightMapPictureBox = new System.Windows.Forms.PictureBox();
            this.btnLoadSprite = new System.Windows.Forms.Button();
            this.nrmlPictureBox = new System.Windows.Forms.PictureBox();
            this.sldHeightToBump = new System.Windows.Forms.TrackBar();
            this.sldScale = new System.Windows.Forms.TrackBar();
            this.btnRegen = new System.Windows.Forms.Button();
            this.btnSaveNormal = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.originalPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightMapPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrmlPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sldHeightToBump)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sldScale)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // originalPictureBox
            // 
            this.originalPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.originalPictureBox.Location = new System.Drawing.Point(12, 12);
            this.originalPictureBox.Name = "originalPictureBox";
            this.originalPictureBox.Size = new System.Drawing.Size(430, 175);
            this.originalPictureBox.TabIndex = 0;
            this.originalPictureBox.TabStop = false;
            // 
            // heightMapPictureBox
            // 
            this.heightMapPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heightMapPictureBox.Location = new System.Drawing.Point(12, 193);
            this.heightMapPictureBox.Name = "heightMapPictureBox";
            this.heightMapPictureBox.Size = new System.Drawing.Size(430, 175);
            this.heightMapPictureBox.TabIndex = 1;
            this.heightMapPictureBox.TabStop = false;
            // 
            // btnLoadSprite
            // 
            this.btnLoadSprite.Location = new System.Drawing.Point(6, 19);
            this.btnLoadSprite.Name = "btnLoadSprite";
            this.btnLoadSprite.Size = new System.Drawing.Size(137, 23);
            this.btnLoadSprite.TabIndex = 2;
            this.btnLoadSprite.Text = "Load File";
            this.btnLoadSprite.UseVisualStyleBackColor = true;
            this.btnLoadSprite.Click += new System.EventHandler(this.btnLoadSprite_Click);
            // 
            // nrmlPictureBox
            // 
            this.nrmlPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nrmlPictureBox.Location = new System.Drawing.Point(448, 193);
            this.nrmlPictureBox.Name = "nrmlPictureBox";
            this.nrmlPictureBox.Size = new System.Drawing.Size(430, 175);
            this.nrmlPictureBox.TabIndex = 3;
            this.nrmlPictureBox.TabStop = false;
            // 
            // sldHeightToBump
            // 
            this.sldHeightToBump.LargeChange = 1;
            this.sldHeightToBump.Location = new System.Drawing.Point(6, 69);
            this.sldHeightToBump.Maximum = 100;
            this.sldHeightToBump.Name = "sldHeightToBump";
            this.sldHeightToBump.Size = new System.Drawing.Size(416, 45);
            this.sldHeightToBump.TabIndex = 4;
            this.sldHeightToBump.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sldHeightToBump.Value = 50;
            // 
            // sldScale
            // 
            this.sldScale.LargeChange = 100;
            this.sldScale.Location = new System.Drawing.Point(6, 120);
            this.sldScale.Maximum = 1000;
            this.sldScale.Name = "sldScale";
            this.sldScale.Size = new System.Drawing.Size(416, 45);
            this.sldScale.SmallChange = 50;
            this.sldScale.TabIndex = 5;
            this.sldScale.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sldScale.Value = 200;
            // 
            // btnRegen
            // 
            this.btnRegen.Location = new System.Drawing.Point(149, 19);
            this.btnRegen.Name = "btnRegen";
            this.btnRegen.Size = new System.Drawing.Size(114, 23);
            this.btnRegen.TabIndex = 6;
            this.btnRegen.Text = "Regenerate";
            this.btnRegen.UseVisualStyleBackColor = true;
            this.btnRegen.Click += new System.EventHandler(this.btnRegen_Click);
            // 
            // btnSaveNormal
            // 
            this.btnSaveNormal.Location = new System.Drawing.Point(269, 19);
            this.btnSaveNormal.Name = "btnSaveNormal";
            this.btnSaveNormal.Size = new System.Drawing.Size(153, 23);
            this.btnSaveNormal.TabIndex = 7;
            this.btnSaveNormal.Text = "Save Normal";
            this.btnSaveNormal.UseVisualStyleBackColor = true;
            this.btnSaveNormal.Click += new System.EventHandler(this.btnSaveNormal_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Pixel Luminance Influence";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Less";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(391, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "More";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Normal Map Scale";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadSprite);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.sldHeightToBump);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.sldScale);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnRegen);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSaveNormal);
            this.groupBox1.Location = new System.Drawing.Point(448, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 175);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 380);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nrmlPictureBox);
            this.Controls.Add(this.heightMapPictureBox);
            this.Controls.Add(this.originalPictureBox);
            this.Name = "Form1";
            this.Text = "Sprite Map";
            ((System.ComponentModel.ISupportInitialize)(this.originalPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightMapPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrmlPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sldHeightToBump)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sldScale)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox originalPictureBox;
        private System.Windows.Forms.PictureBox heightMapPictureBox;
        private System.Windows.Forms.Button btnLoadSprite;
        private System.Windows.Forms.PictureBox nrmlPictureBox;
        private System.Windows.Forms.TrackBar sldHeightToBump;
        private System.Windows.Forms.TrackBar sldScale;
        private System.Windows.Forms.Button btnRegen;
        private System.Windows.Forms.Button btnSaveNormal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

