﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpriteMap
{
    public partial class Form1 : Form
    {

        private class WorkerOptions
        {
            public int NormalScale { get; set; }
            public int HeightToBump { get; set; }
        }

        BusyForm busy = new BusyForm();
        BackgroundWorker bw = new BackgroundWorker();
        Image heightMap, nrmlMap, sprite;

        public Form1()
        {
            InitializeComponent();
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            heightMapPictureBox.Image = heightMap;
            nrmlPictureBox.Image = nrmlMap;
            originalPictureBox.Image = sprite;
            busy.Close();
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            
            var bw = sender as BackgroundWorker;
            var opts = e.Argument as WorkerOptions;
            var gen = new HeightMapGenerator();
            heightMap = gen.GenerateHeightMap(sprite, (float)opts.HeightToBump / 100f);
            var nrmlGen = new NormalMapGenerator();
            nrmlMap = nrmlGen.GenerateNormalMap(heightMap, (float)opts.NormalScale / 100f);
        }

        void generate()
        {
            busy = new BusyForm();
            busy.Show();
            bw.RunWorkerAsync(new WorkerOptions
            {
                HeightToBump = sldHeightToBump.Value,
                NormalScale = sldScale.Value
            });
        }

        private void btnLoadSprite_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.ShowDialog();
            if (!String.IsNullOrEmpty(dialog.FileName))
            {
                var img = Image.FromFile(dialog.FileName);
                sprite = img;
                generate();
            }
        }

        private void btnRegen_Click(object sender, EventArgs e)
        {
            if (originalPictureBox.Image != null)
                generate();
        }

        private void btnSaveNormal_Click(object sender, EventArgs e)
        {
            if (nrmlPictureBox.Image != null)
            {
                var dialog = new SaveFileDialog();
                dialog.ShowDialog();
                if (!String.IsNullOrEmpty(dialog.FileName))
                {
                    nrmlPictureBox.Image.Save(dialog.FileName);
                }
            }
        }
    }
}
