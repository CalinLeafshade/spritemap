﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nexus;

namespace SpriteMap
{
    public class NormalMapGenerator
    {

        Bitmap currentHeightMap;

        bool valid(int x, int y)
        {
            return x >= 0 && x < currentHeightMap.Width && y >= 0 && y < currentHeightMap.Height;
        }

        float? getArrVal(float[] ar, int cx, int cy, int dx, int dy)
        {
            int x = cx + dx;
            int y = cy + dy;
            if (valid(x, y))
            {
                return currentHeightMap.GetPixel(x, y).GetBrightness();
            }
            else
            {
                return null;
            }
        }

        float[] getLocalMatrix(int cx, int cy)
        {
            float[] result = new float[9];
            Color c = currentHeightMap.GetPixel(cx, cy);
            for (int i = 0; i < 9; i++)
            {
                result[i] = c.GetBrightness();
            }

            result[0] = getArrVal(result, cx, cy, -1, 1) ?? result[4];
            result[1] = getArrVal(result, cx, cy, 0, 1) ?? result[4];
            result[2] = getArrVal(result, cx, cy, 1, 1) ?? result[4];
            result[3] = getArrVal(result, cx, cy, -1, 0) ?? result[4];
            result[5] = getArrVal(result, cx, cy, 1, 0) ?? result[4];
            result[6] = getArrVal(result, cx, cy, -1, -1) ?? result[4];
            result[7] = getArrVal(result, cx, cy, 0, -1) ?? result[4];
            result[8] = getArrVal(result, cx, cy, 1, -1) ?? result[4];

            return result;

        }

        Vector3D getVectorFromMatrix(float[] s, float scale = 1f)
        {
            Vector3D n = new Vector3D();
            n.X = scale * -(s[2] - s[0] + 2 * (s[5] - s[3]) + s[8] - s[6]);
            n.Y = scale * -(s[6] - s[0] + 2 * (s[7] - s[1]) + s[8] - s[2]);
            n.Z = 1f;
            n.Normalize();
            return n;
        }

        Color colFromVector(Vector3D v, int alpha)
        {
            alpha = 255;
            return Color.FromArgb(alpha, (int)((v.X / 2f) * 255) + 128, (int)((v.Y / 2f) * 255) + 128, (int)((v.Z / 2f) * 255) + 128);
        }

        public Image GenerateNormalMap(Image heightMap, float scale = 1f)
        {
            currentHeightMap = (Bitmap)heightMap;

            var normalMap = new Bitmap(heightMap.Width, heightMap.Height);

            for (int x = 0; x < currentHeightMap.Width; x++)
            {
                for (int y = 0; y < currentHeightMap.Height; y++)
                {
                    float[] localMatrix = getLocalMatrix(x, y);
                    var normal = getVectorFromMatrix(localMatrix, scale);
                    normalMap.SetPixel(x, y, colFromVector(normal, currentHeightMap.GetPixel(x,y).B));
                }
            }

            return (Image)normalMap;

        }
    }
}
